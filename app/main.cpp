#include "student.h"
#include "whitelist.h"
#include <iostream>
void enter_data_into_whitelist(whitelist &student_list);

int main()
{

    std::string name;
    whitelist student_list;
    student *stu_ptr;

    enter_data_into_whitelist(student_list);

    std::cout << "\n\n\n========================================\n";
    std::cout << "\tAdvanced C++ Concepts Task\n";
    std::cout << "========================================\n";
    std::cout << "Enter name to search from whitelist: ";
    std::cin >> name;

    if (student_list.is_student_present(name))
    {
        stu_ptr = student_list.get_student_data(name);

        std::cout << "\nRoll no. of " << name << " : " << stu_ptr->get_roll_no() << std::endl;
        std::cout << "Age of " << name << " : " << stu_ptr->get_age() << std::endl;
        std::cout << "CGPA of " << name << " : " << stu_ptr->get_cgpa() << std::endl;
        stu_ptr->set_subject_marks("English", 50);
        stu_ptr->set_subject_marks("Urdu", 60);
        stu_ptr->print_all_marks();
    }
    else
    {
        std::cout << "Name ' " << name << " ' not whitelisted\n";
    }
    std::cout << "\n========================================\n";
}

void enter_data_into_whitelist(whitelist &student_list)
{

    student stu_1("170488", 22, 4);
    student_list.add_to_whitelist("Jawad", stu_1);
    stu_1.set("170401", 22, 3.6);
    student_list.add_to_whitelist("Adil", stu_1);
    stu_1.set("160421", 25, 2.5);
    student_list.add_to_whitelist("Ali", stu_1);
    stu_1.set("4", 4, 4);
    student_list.add_to_whitelist("Abdullah", stu_1);
    stu_1.set("5", 5, 4);
    student_list.add_to_whitelist("Umair", stu_1);
    stu_1.set("6", 6, 4);
    student_list.add_to_whitelist("Usman", stu_1);
    stu_1.set("7", 7, 2);
    student_list.add_to_whitelist("Usama", stu_1);
    stu_1.set("8", 8, 2);
    student_list.add_to_whitelist("Ahamed", stu_1);
    stu_1.set("9", 9, 2);
    student_list.add_to_whitelist("Talha", stu_1);
    stu_1.set("10", 10, 2);
    student_list.add_to_whitelist("Arim", stu_1);
    stu_1.set("11", 11, 2);
    student_list.add_to_whitelist("Haris", stu_1);
    stu_1.set("12", 12, 2);
    student_list.add_to_whitelist("Farid", stu_1);
    stu_1.set("13", 13, 2);
    student_list.add_to_whitelist("Hamza", stu_1);
    stu_1.set("14", 2, 2);
    student_list.add_to_whitelist("Yasir", stu_1);
    stu_1.set("15", 2, 2);
    student_list.add_to_whitelist("Munib", stu_1);

    student_list.add_to_whitelist("Haris", stu_1);
    stu_1.set("12", 12, 2);
}