// Contains all the Functions of student.h

#include "student.h"
#include <iostream>
using namespace std;

student::student()
{
}

student::student(const std::string &roll_no, const int &age, const float &cgpa)
{
    data.roll_no = "roll_no";
    data.roll_no = roll_no;
    data.age = age;
    data.cgpa = cgpa;
};

student::~student()
{
}

void student::set(const std::string &roll_no, const int &age, const float &cgpa)
{
    data.roll_no = roll_no;
    data.age = age;
    data.cgpa = cgpa;
};

const std::string student::get_roll_no() const
{
    return data.roll_no;
};

const int student::get_age() const
{
    return data.age;
};

const float student::get_cgpa() const
{
    return data.cgpa;
};

const int student::get_subject_marks(const std::string &subject)
{
    std::map<std::string, int>::iterator it1;
    it1 = result.find(subject);
    if (it1 != result.end())
    {

        return it1->second;
    }
    // -1 means subject not found
    return -1;
};

void student::set_subject_marks(const std::string &subject, const int &marks)
{
    result[subject] = marks;
};

void student::print_all_marks()
{
    if (result.empty())
    {
        std::cout << "\n\nNo Marks to display\n";
    }
    else
    {
        std::cout << "\n\n--------------- Dispay All Marks ---------------\n";
        std::map<std::string, int>::iterator it1;
        for (it1 = result.begin(); it1 != result.end(); ++it1)
        {
            std::cout << it1->first << ":    " << it1->second << "\n";
        }
    }
    return;
};