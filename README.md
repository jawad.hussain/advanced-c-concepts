# Advance C++ Concepts

## Discription:
This code demonstrates the use of different STL containers, header guards and overall optimization practices like const.

## How to run:
- First Clone the repository:
  
  ```
    git clone https://gitlab.com/jawad.hussain/advanced-c-concepts.git
  ```
- Then Run the following commands from root of project directory:
  
  ```
    mkdir build
    
    cd build/
    
    cmake ..
    
    make

    ./app/main
  ```

## Important Note:

The task required us to store student_data_list in a vector STL container. But the issue is that the container resizes with the increase in data and when resizing the memory addresses tend to change. Due to this any pointer pointing to specific data in the vector container has a possibility of returning garbage values.    
To fix this issue there were 3 approches that could be followed:   
1. Change the vector container with another container like list or dequeue.
2. Reserve size for vector using `vector.reserve()`.
3. Don't store pointer to vector elements in `student_record`. Instead store index number.

I followed the first approch and changed the container to **list**.