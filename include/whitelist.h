#ifndef WHITELIST_H
#define WHITELIST_H

#include "student.h"
#include <list>

//#include <vector>

class whitelist
{
private:
    std::map<std::string, student *> student_record;
    // std::vector<student> student_data_list;     // To be Removed after review
    std::list<student> student_data_list;

public:
    // whitelist();                                 // To be Removed after review
    void add_to_whitelist(const std::string &, const student &);
    const bool is_student_present(const std::string &) const;
    student *get_student_data(const std::string &) const;
};

#endif