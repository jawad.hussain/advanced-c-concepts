#ifndef STUDENT_H
#define STUDENT_H

#include <map>
#include <string>
class student
{
private:
    struct student_record
    {
        std::string roll_no;
        int age;
        float cgpa;
    };
    student_record data;
    std::map<std::string /*subject name*/, int /*marks*/> result;

public:
    student();
    student(const std::string &roll_no, const int &age, const float &cgpa);

    void set(const std::string &roll_no, const int &age, const float &cgpa);
    const std::string get_roll_no() const;
    const int get_age() const;
    const float get_cgpa() const;
    const int get_subject_marks(const std::string &subject);
    void set_subject_marks(const std::string &subject, const int &marks);
    void print_all_marks();
    ~student();
};

#endif